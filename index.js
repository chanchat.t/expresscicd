const express = require('express');
const app = express();
const port = 3000;
app.disable('x-powered-by');
app.get('/', (req, res) => {
    res.send('Hello World! Test image pull kubernetes 08/02 Real Final test');
});
app.post('/', (req, res) => {
    res.send('Got a POST request');
});
app.delete('/', (req, res) => {
    res.send('Got a DELETE request');
});
app.put('/', (req, res) => {
    res.send('Got a PUT request');
});
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
